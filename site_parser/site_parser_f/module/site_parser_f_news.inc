<?php

/**
 * Изображение
 */ 
function parcer_news_images($doc){
  $images = '';
  drupal_alter('parcer_news_images', $images, $doc);
  return $images;
}

/**
 * Дата и время 
 */
function parcer_news_date($doc){
  $date = '';
  drupal_alter('parcer_news_date', $date, $doc);
  return $date;
}

/**
 * Занесение EXTRA полей
 */
function parser_news_extra($doc){
  $extra = '';
  drupal_alter('parcer_news_extra', $extra, $doc);
  return $extra;
}