<?php

/**
 * ID сущности на сайте источнике. 
 * Этот ID будет сохранён в бд (таблица parser_map) и позволит избежать дублей сущностей, доступных по нескольким URL. 
 * Так же, зная этот ID, можно с помощью функции parser_get_entity_id_by_remote_id() получить ID созданной сущности. 
 * Поле является не обязательным, но крайне рекомендованное к заполнению.
 */
function parser_id($doc){ 
  $id = '';
  drupal_alter('parser_id', $id, $doc);
  return $id;
}

/**
 * NODE TITLE
 */
function parser_title($doc){
  $title = '';
  drupal_alter('parser_title', $title, $doc);
  return $title;
}

/**
 * NODE UID
 */
function parser_uid($doc){
  $uid = 1;
  drupal_alter('parser_uid', $uid, $doc);
  return $uid;
}