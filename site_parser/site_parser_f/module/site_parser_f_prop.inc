<?php
/**
 * Валюта
 * по умолчанию вернет default валюту сайта
 */
function parcer_prop_currency($doc){
  $currency = variable_get('pr_price_user_currency_default', 'USD');
  drupal_alter('parcer_prop_currency', $currency, $doc);
  return $currency;
}

/**
 * Местоположение
 */
function parcer_prop_location($doc){
  $location = '';
  drupal_alter('parcer_prop_location', $location, $doc);
  return $location;
}

/**
 * Тип Недвижимости
 */
function parcer_prop_type($doc){
  $type = '';
  drupal_alter('parcer_prop_type', $type, $doc);
  return $type;
}

/**
 * Номер Лота
 */
function parcer_prop_lot($doc){
  $lot = '';
  drupal_alter('parcer_prop_lot', $lot, $doc);
  return $lot;
}

function parcer_prop_address($doc){
  $address = '';
  drupal_alter('parcer_prop_address', $address, $doc);
  return $address;
}

/**
 * Комнаты
 */
function parcer_prop_rooms($doc){
  $rooms = '';
  drupal_alter('parcer_prop_rooms', $rooms, $doc);
  //если комнаты есть
  if( (!empty($rooms)) && ((int)$rooms != 0) ){
	//получаем спикок данных поля
	$options = prProperty::getRoomsArray();
	//полчаем ключ последнего елемент массива
	$count_rooms = end(array_flip($options));
	//если комнат больше или равно этому ключу, тогда возвращяем 5+
	if($rooms >= $count_rooms){
	  $rooms = $count_rooms;
	}
  }
  return $rooms;
}

/**
 * Этаж
 */
function parcer_prop_floor($doc){
  $floor = '';
  drupal_alter('parcer_prop_floor', $floor, $doc);
  if(!empty($floor)){
	return (int)$floor;
  }
}

/**
 * Этажность
 */
function parcer_prop_floors($doc){
  $floors = '';
  drupal_alter('parcer_prop_floors', $floors, $doc);
  if(!empty($floors)){
	return (int)$floors;
  }
}

/**
 * Площадь
 */
function parcer_prop_area($doc){
  $area = '';
  drupal_alter('parcer_prop_area', $area, $doc);
  return $area;
}

/**
 * Ремонт
 */
function parcer_prop_renovation($doc){
  $renovation = '';
  drupal_alter('parcer_prop_renovation', $renovation, $doc);
  return $renovation;
}

/**
 * Балкон
 */
function parcer_prop_balcony($doc){
  $balcony = '';
  drupal_alter('parcer_prop_balcony', $balcony, $doc);
  return $balcony;
}

/**
 * Лоджия
 */ 
function parcer_prop_loggia($doc){
  $loggia = '';
  drupal_alter('parcer_prop_loggia', $loggia, $doc);
  return $loggia;
}

/**
 * Изображение
 */ 
function parcer_prop_images($doc){
  $images = '';
  drupal_alter('parcer_prop_images', $images, $doc);
  return $images;
}

/**
 * Описание
 */ 
function parcer_prop_descr($doc){
  $descr = '';
  drupal_alter('parcer_prop_descr', $descr, $doc);
  return $descr;
}

/**
 * Цена
 */ 
function parcer_prop_price($doc){
  $price = '';
  drupal_alter('parcer_prop_price', $price, $doc);
  return $price;
}
/**
 * Цена за неделю
 */ 
function parcer_prop_price_week($doc){
  $price_week = '';
  drupal_alter('parcer_prop_price_week', $price_week, $doc);
  return $price_week;
}

/**
 * Цена за месяц
 */ 
function parcer_prop_price_month($doc){
  $price_month = '';
  drupal_alter('parcer_prop_price_month', $price_month, $doc);
  return $price_month;
}

/**
 * Получаем координаты и записываем в поля Широты и Долготы
 */
function parcer_prop_gcoordinates($doc, &$entity){
  $adress = '';
  drupal_alter('parcer_prop_gcoordinates', $adress, $doc, $entity);
  $url = 'http://maps.google.com/maps/api/geocode/json?address='.urlencode($adress).'&sensor=false';
  $result_request = drupal_http_request($url);
  if($result_request->code == 200){
	$gprs = drupal_json_decode($result_request->data);
	$coordinates = $gprs['results'][0]['geometry']['location'];
	if(!empty($coordinates)){
	  $entity->field_property_maps_lat['und'][0]['value'] = $coordinates['lat'];
	  $entity->field_property_maps_lng['und'][0]['value'] = $coordinates['lng'];
	}
  }
}

/**
 * Получение данных для мета
 */
function parcer_prop_node_meta($page, &$entity){
  $meta = new stdClass();
  drupal_alter('parcer_prop_meta', $meta, $page);
  $meta->path = 'node/' . $entity->nid;//путь к страницу
  if(!isset($meta->language)){
	$meta->language = language_default();
  }
  if(isset($meta->data['keywords'])){
	$meta->data['keywords'] = truncate_utf8($meta->data['keywords'], 255, TRUE, FALSE);
  }
  
  simplemeta_meta_save($meta);
}

/**
 * Занесение EXTRA полей
 */
function parser_prop_extra($doc){
  
  $extra = array();
  drupal_alter('parser_prop_extra', $extra, $doc);
  return serialize($extra);
  
}

/**
 * Особенности
 */
function parcer_prop_features($doc){
  $features = '';
  drupal_alter('parcer_prop_features', $features, $doc);
  return $features;
}

/**
 * Площадь участка в сотках
 */
function parcer_prop_area_land_ar($doc){
  $land_area = '';
  drupal_alter('parcer_prop_area_land_ar', $land_area, $doc);
  return $land_area;
}

/**
 * Площадь участка в метрах
 */
function parcer_prop_area_land_sqm($doc){
  $land_area = '';
  drupal_alter('parcer_prop_area_land_sqm', $land_area, $doc);
  return $land_area;
}

/**
 * Bathrooms
 */
function parcer_prop_bathrooms($doc){
  $bathrooms = '';
  drupal_alter('parcer_prop_bathrooms', $bathrooms, $doc);
  if( (!empty($bathrooms)) && ((int)$bathrooms != 0) ){
	//получаем спикок данных поля
	$options = prProperty::getBathRoomsArray();
	//полчаем ключ последнего елемент массива
	$count_rooms = end(array_flip($options));
	//если комнат больше или равно этому ключу, тогда возвращяем 5+
	if($bathrooms >= $count_rooms){
	  $bathrooms = $count_rooms;
	}
  }
  return $bathrooms;
}

/**
 * Distance to airport
 */
function parcer_prop_distance_to_air($doc){
  $distance_to_air = '';
  drupal_alter('parcer_prop_distance_to_air', $distance_to_air, $doc);
  return $distance_to_air;
}

/**
 * Distance to sea
 */
function parcer_prop_distance_to_sea($doc){
  $distance_to_sea = '';
  drupal_alter('parcer_prop_distance_to_sea', $distance_to_sea, $doc);
  return $distance_to_sea;
}

/**
 * Label
 */
function parcer_prop_label($doc){
  $label = '';
  drupal_alter('parcer_prop_label', $label, $doc);
  return $label;
}

/**
 * Latitude
 */
function parcer_prop_lat($doc){
  $lat = '';
  drupal_alter('parcer_prop_lat', $lat, $doc);
  return $lat;
}

/**
 * Latitude
 */
function parcer_prop_lng($doc){
  $lng = '';
  drupal_alter('parcer_prop_lng', $lng, $doc);
  return $lng;
}

/**
 * Sleeps
 */
function parcer_prop_sleeps($doc){
  $sleeps = '';
  drupal_alter('parcer_prop_sleeps', $sleeps, $doc);
  if( (!empty($sleeps)) && ((int)$sleeps != 0) ){
	return $sleeps;
  }
}