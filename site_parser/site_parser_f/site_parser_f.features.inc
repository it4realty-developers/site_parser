<?php
/**
 * @file
 * site_parser_f.features.inc
 */

/**
 * Implements hook_default_parser_job().
 */
function site_parser_f_default_parser_job() {
  $items = array();
  $items['2'] = entity_import('parser_job', '{
    "jid" : "2",
    "title" : "Property",
    "start_url" : "http:\\u002F\\u002Fwww.krona-zagran.com.ua\\u002Fproperties\\u002Fall\\u002Fflat?page=[mask:0,43]\\u000D\\u000Ahttp:\\u002F\\u002Fwww.krona-zagran.com.ua\\u002Fproperties\\u002Fall\\u002Fhouse?page=[mask:0,3]\\u000D\\u000Ahttp:\\u002F\\u002Fwww.krona-zagran.com.ua\\u002Fproperties\\u002Fall\\u002Fplot\\u000D\\u000Ahttp:\\u002F\\u002Fwww.krona-zagran.com.ua\\u002Fproperties\\u002Fall\\u002Fcommercial?page=[mask:0,4]",
    "test_url" : "http:\\u002F\\u002Fkrona-zagran.com.ua\\u002Fhouse\\u002Fdve-villy-v-okrestnostyah-bratislavy",
    "only_this_domen" : "1",
    "depth" : "1",
    "parse_limit" : "635",
    "white_list" : "http:\\u002F\\u002Fwww.krona-zagran.com.ua\\u002Fflat\\u002F*\\u000D\\u000Ahttp:\\u002F\\u002Fwww.krona-zagran.com.ua\\u002Fhouse\\u002F*\\u000D\\u000Ahttp:\\u002F\\u002Fwww.krona-zagran.com.ua\\u002Fplot\\u002F*\\u000D\\u000Ahttp:\\u002F\\u002Fwww.krona-zagran.com.ua\\u002Fcommercial\\u002F*",
    "black_list" : "http:\\u002F\\u002Fwww.krona-zagran.com.ua\\u002Fflag*\\u000D\\u000A*.jpg\\u000D\\u000A*.png\\u000D\\u000A*.zip\\u000D\\u000A*.rar\\u000D\\u000A*.pdf",
    "check_code" : "if(\\u000D\\u000A  $doc-\\u003Efind(\\u0027body\\u0027)-\\u003EhasClass(\\u0027node-type-flat\\u0027) ||\\u000D\\u000A  $doc-\\u003Efind(\\u0027body\\u0027)-\\u003EhasClass(\\u0027node-type-house\\u0027) ||\\u000D\\u000A  $doc-\\u003Efind(\\u0027body\\u0027)-\\u003EhasClass(\\u0027node-type-plot\\u0027) ||\\u000D\\u000A  $doc-\\u003Efind(\\u0027body\\u0027)-\\u003EhasClass(\\u0027node-type-commercial\\u0027)\\u000D\\u000A){\\u000D\\u000A  return 1;\\u000D\\u000A}",
    "save_url" : "0",
    "no_update" : "0",
    "site_charset" : "auto",
    "charset_fix" : "0",
    "entity_type" : "node",
    "bundle" : "property",
    "remote_id_code" : "return parser_id($doc);",
    "fields_code" : {
      "title" : "return parser_title($doc);",
      "author" : "return parser_uid($doc);",
      "field_extra" : "return parser_prop_extra($doc);",
      "field_property_area" : "return parcer_prop_area($doc);",
      "field_property_area_land" : "return parcer_prop_area_land($doc);",
      "field_property_balcony" : "return parcer_prop_balcony($doc);",
      "field_property_floor" : "return parcer_prop_floor($doc);",
      "field_property_floors_total" : "return parcer_prop_floors($doc);",
      "field_property_images" : "return parcer_prop_images($doc);",
      "field_property_location" : "return parcer_prop_location($doc);",
      "field_property_loggia" : "return parcer_prop_loggia($doc);",
      "field_property_renovation" : "return parcer_prop_renovation($doc);",
      "field_property_rooms" : "return parcer_prop_rooms($doc);",
      "field_property_type" : "return parcer_prop_type($doc);",
      "field_property_price_enter_from" : "return parcer_prop_price($doc);",
      "field_property_price_in_currency" : "return parcer_prop_currency($doc);",
      "field_property_features" : "return parcer_prop_features($doc);",
      "field_property_lot" : "return parcer_prop_lot($doc);"
    },
    "list_mode_code" : "",
    "init_code" : "",
    "prepare_code" : "parcer_prop_gcoordinates($doc, $entity);\\u000D\\u000Aparcer_prop_node_meta($page, $entity);\\u000D\\u000A$entity-\\u003Efield_property_type[\\u0027und\\u0027][0][\\u0027tid\\u0027] = pr_local_settings_f_parcer_prop_type($doc);\\u000D\\u000A\\u000D\\u000A",
    "headers" : "User-Agent: Opera\\u002F9.80 (Windows NT 6.1; U; ru) Presto\\u002F2.8.131 Version\\u002F11.10",
    "proxies" : "",
    "force_download" : "0",
    "force_download_list" : "",
    "sleep" : "0",
    "run_period" : "0",
    "last_run" : "0",
    "status" : "3",
    "module" : "site_parser_f"
  }');
  $items['3'] = entity_import('parser_job', '{
    "jid" : "3",
    "title" : "News",
    "start_url" : "http:\\u002F\\u002Fwww.krona-zagran.com.ua\\u002Fnews?page=[mask:0,11]\\u000D\\u000Ahttp:\\u002F\\u002Fwww.krona-zagran.com.ua\\u002Finfo?page=[mask:0,2]",
    "test_url" : "http:\\u002F\\u002Fwww.krona-zagran.com.ua\\u002Farticles\\u002Falbaniya-tonkosti-pokupki-nedvizhimosti",
    "only_this_domen" : "1",
    "depth" : "1",
    "parse_limit" : "147",
    "white_list" : "http:\\u002F\\u002Fwww.krona-zagran.com.ua\\u002Fnews\\u002F*\\u000D\\u000Ahttp:\\u002F\\u002Fwww.krona-zagran.com.ua\\u002Farticles\\u002F*\\u000D\\u000A",
    "black_list" : "*.jpg\\u000D\\u000A*.png\\u000D\\u000A*.zip\\u000D\\u000A*.rar\\u000D\\u000A*.pdf",
    "check_code" : "if(\\u000D\\u000A  ($doc-\\u003Efind(\\u0027body\\u0027)-\\u003EhasClass(\\u0027node-type-news\\u0027)) ||\\u000D\\u000A  ($doc-\\u003Efind(\\u0027body\\u0027)-\\u003EhasClass(\\u0027node-type-infomaterial\\u0027))\\u000D\\u000A){\\u000D\\u000A  return 1;\\u000D\\u000A} \\u000D\\u000A",
    "save_url" : "0",
    "no_update" : "0",
    "site_charset" : "auto",
    "charset_fix" : "0",
    "entity_type" : "node",
    "bundle" : "infomaterial",
    "remote_id_code" : "return parser_id($doc);",
    "fields_code" : {
      "title" : "return parser_title($doc);",
      "author" : "return parser_uid($doc);",
      "field_extra" : "return parser_news_extra($doc);",
      "field_infomaterial_date" : "return parcer_news_date($doc);",
      "field_infomaterial_front" : "return 0;",
      "field_infomaterial_images" : "return parcer_news_images($doc);",
      "field_infomaterial_sort" : "return 0;",
      "field_infomaterial_type" : "if($doc-\\u003Efind(\\u0027body\\u0027)-\\u003EhasClass(\\u0027node-type-news\\u0027)){\\u000D\\u000A  return \\u0027news\\u0027;\\u000D\\u000A}\\u000D\\u000Aif($doc-\\u003Efind(\\u0027body\\u0027)-\\u003EhasClass(\\u0027node-type-infomaterial\\u0027)){\\u000D\\u000A  return \\u0027articles\\u0027;\\u000D\\u000A}  \\u000D\\u000A"
    },
    "list_mode_code" : "",
    "init_code" : "",
    "prepare_code" : "parcer_prop_node_meta($page, $entity);",
    "headers" : "User-Agent: Opera\\u002F9.80 (Windows NT 6.1; U; ru) Presto\\u002F2.8.131 Version\\u002F11.10\\u000D\\u000A",
    "proxies" : "",
    "force_download" : "0",
    "force_download_list" : "",
    "sleep" : "0",
    "run_period" : "0",
    "last_run" : "0",
    "status" : "3",
    "module" : "site_parser_f"
  }');
  return $items;
}
